public class ListItem<T> {
    // Constructor

    public ListItem(T item) {
        this.item = item; // Store the item
        next = null; // Set next as end point
    }

    // Return class name & object
    @Override
    public String toString() {
        return "ListItem " + item;
    }

    ListItem<T> next; // Refers to next item in the list
    T item;
}
