public class ListItem {
    // Constructor
    public ListItem(Object item) {
        this.item = item; // Store the item
        next = null; // Set next as end point
    }

    // Return class name & object
    @Override
    public String toString() {
        return "ListItem " + item;
    }

    ListItem next; // Refers to next item in the list
    Object item;
}
