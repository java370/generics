public class SimpleBoundedType {

    // array time values sum calculation
    public static class SumArray<T extends Number> {
        private double totalSum = 0.0;
        T[] arr;

        public SumArray(T[] arr) {
            this.arr = arr;
        }

        public void CalcSum() {
            for (T t : arr) {
                totalSum += t.doubleValue();
            }
        }

        public double getSum() {
            return totalSum;
        }

    }

    // any numbers integer, float, doubles square findings
    public static class NumericSquare<T extends Number> {
        private T value;

        public NumericSquare(T value) {
            this.value = value;
        }

        public double getSquare() {
            return this.value.doubleValue() * this.value.doubleValue();
        }

    }

    public static void main(String[] args) {
        NumericSquare<Integer> v1 = new NumericSquare<>(1256);
        System.out.println(v1.getSquare());

        NumericSquare<Double> v2 = new NumericSquare<>(15.6);
        System.out.println(v2.getSquare());






        // sum of integer arrays
        Integer[] krr = { 12, 5, 6 };
        SumArray<Integer> IntSum = new SumArray<>(krr);
        IntSum.CalcSum();
        System.out.println(IntSum.getSum());

        // sum of double arrays
        Double[] lrr = { 1.2, 5.8, 7.0 };
        SumArray<Double> DblSum = new SumArray<>(lrr);
        DblSum.CalcSum();
        System.out.println(DblSum.getSum());

    }

}
