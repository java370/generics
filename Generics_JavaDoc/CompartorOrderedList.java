public class CompartorOrderedList {
    public static class MyUtil {
        public <K, V> boolean MyComparator(myPair<K, V> p1, myPair<K, V> p2) {

            return ((p1.getValue()).equals(p2.getValue()));
            // return (p1.getKey().equals(p2.getKey())) &&
            // (p1.getValue().equals(p2.getValue()));
        }
    }

    public static class myPair<K, V> {

        private K key;
        private V value;

        public myPair(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public K getKey() {
            return key;
        }

        public V getValue() {
            return value;
        }
    }

    public static void main(String[] args) {
        myPair<Integer, String> mp1 = new myPair<>(2, "Sajal");
        myPair<Integer, String> mp2 = new myPair<>(8, "sajal");
        MyUtil cmp = new MyUtil();
        if (cmp.MyComparator(mp1, mp2)) {
            System.out.println(mp1.getValue() + " == " + mp2.getValue());
        } else {
            System.out.println(mp1.getValue() + " != " + mp2.getValue());

        }
    }

}
