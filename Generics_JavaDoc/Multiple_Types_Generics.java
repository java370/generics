public class Multiple_Types_Generics {

    public interface pair<K, V> {
        public K getKey();

        public V getValue();
    }

    public static class MyOrderedList<K, V> implements pair<K, V> {
        private K key;
        private V value;

        public MyOrderedList(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public K getKey() {
            return this.key;

        }

        public V getValue() {
            return this.value;
        }

    }

    public static void main(String[] args) {
        MyOrderedList<String, Integer> od1 = new MyOrderedList<>("sajal", 84848);
        MyOrderedList<String, Integer> od2 = new MyOrderedList<>("y_asha", 12548);
        MyOrderedList<String, Integer> od3 = new MyOrderedList<>("farzana", 788);

        System.out.println(od1.getKey() + " -- " + od1.getValue());
        System.out.println(od2.getKey() + " -- " + od2.getValue());
        System.out.println(od3.getKey() + " -- " + od3.getValue());

    }

}
