public class SimpleGenericClass<T> {
    private T t;

    public T getValue() {
        return this.t;
    }

    public void setValue(T t) {
        this.t = t;
    }
}

