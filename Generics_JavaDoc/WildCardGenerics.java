import java.util.ArrayList;
import java.util.List;

public class WildCardGenerics {

    public static class ShowData {

        public void ListData(List<?> obj) {
            for (Object object : obj) {
                System.out.println(object);
            }
        }

        // new method for merging two different lists
        // Return merged two list with a single list 
        public List<Object> ListData(List<?> a, List<?> b) {
            // a.addAll(b);
            List<Object> set = new ArrayList<>(a);
            set.addAll(b);
            return set;
        }

    }

    public static void main(String[] args) {

        // Make a Class to show any type of List
        ShowData showData = new ShowData();

        // string type data
        List<String> ls = new ArrayList<>();
        ls.add("Sajal");
        ls.add("uhfiud");
        ls.add("erfsdf");
        ls.add("njfas");
        showData.ListData(ls);

        // Integer Type data
        List<Integer> it = new ArrayList<>();
        it.add(1);
        it.add(5);
        it.add(7);
        it.add(9);
        it.add(4);

        showData.ListData(it);

        // List of List printing with this method
        ArrayList<Integer> at = new ArrayList<>();
        at.add(454);
        at.add(454);
        at.add(454);
        at.add(454);

        ArrayList<Integer> at2 = new ArrayList<>();
        at2.add(58);
        at2.add(58);
        at2.add(58);
        at2.add(58);

        List<ArrayList<Integer>> listOfList = new ArrayList<>();
        listOfList.add(at);
        listOfList.add(at2);

        showData.ListData(listOfList);

        // Merge two same type arrayList [at] and [at2]
        List<Object> mergedTwoList = new ArrayList<>(showData.ListData(at, at2));
        System.out.println(mergedTwoList);

        // Merge two different type arrayList [gh] and [gh2]
        ArrayList<String> gh = new ArrayList<>();
        gh.add("Sajal");
        gh.add("uhfiud");
        gh.add("erfsdf");
        gh.add("njfas");

        ArrayList<Integer> gh2 = new ArrayList<>();
        gh2.add(78);
        gh2.add(68);
        gh2.add(38);
        gh2.add(28);

        List<Object> mergedTwoList2 = new ArrayList<>(showData.ListData(gh, gh2));
        System.out.println(mergedTwoList2);

    }

}
