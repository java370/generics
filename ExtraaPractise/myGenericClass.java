public class myGenericClass<T> {
    T x;

    public myGenericClass(T x) {
        this.x = x;
    }

    public T value_get() {
        return this.x;
    }
}
