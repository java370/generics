// takes two generic parameters

public class gen2<K, V> {
    K k;
    V v;

    public gen2(K k, V v) {
        this.k = k;
        this.v = v;

    }

    // returns two generic parameters
    public gen2<K, V> getB() {
        return this;
    }

}
