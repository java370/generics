public static List<Cake> union(List<? extends Cake> list1, List<? extends Cake> list2) {
        List<Cake> set = new ArrayList<>(list1);
        set.addAll(list2);
        return set;
    }
