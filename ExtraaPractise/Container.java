public class Container <i1, i2> {
    i1 o1;
    i2 o2;

    public Container(i1 o1, i2 o2) {
        this.o1 = o1;
        this.o2 = o2;
    }

    public void print_me() {
        System.out.println("Object = " + o1);
        System.out.println("Object = " + o2);
    }

    public i1 getO1() {
        return o1;
    }

    public void setO1(i1 o1) {
        this.o1 = o1;
    }

    public i2 getO2() {
        return o2;
    }

    public void setO2(i2 o2) {
        this.o2 = o2;
    }
}